import java.util.Scanner;
public class Main {

    public static void main(String [] args){
        /**
         * Creating local variables
         */
        int n0 = 1;
        int n1 = 1;
        int n2 = 0;
        int fromA;
        int toB;
        int countOdd = 2;
        int countEven = 0;
        int N;
        int countFOdd = 0;
        int countFEven = 0;
        Scanner sc = new Scanner(System.in);
        fromA = sc.nextInt();
        toB = sc.nextInt();
        /**
         * Printing odd numbers in right way
         */
        for(int i = fromA; i <= toB; i++){
            if(i%2 == 1){
                System.out.println(i);
                countOdd+=i;
            }
        }
        /**
         * Printing even numbers in wrong way
         */
        System.out.println(countOdd);
        for(int i = toB; i >= fromA; i--){
            if(i%2 == 0) {
                System.out.println(i);
                countEven+=i;
            }
        }
        System.out.println(countEven);
        N = sc.nextInt();
        /**
         * Counting Fibonacci numbers odd and even
         */
        for(int i = 3; n2 < N; i++){
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
            if(n2%2 == 1){
                countFOdd++;
            } else {
                countFEven++;
            }
        }
        /**
         * Printing percentage odd/even
         */
        System.out.println(countFOdd/countFEven * 100);
    }
}
